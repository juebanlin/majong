package net.jueb.majong.gate;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;

import net.jueb.majong.core.util.JProperties;
import net.jueb.majong.core.util.Log4jUtil;
import net.jueb.util4j.common.game.env.Environment;
import net.jueb.util4j.common.game.env.Environment.EnvironmentType;
import net.jueb.util4j.hotSwap.classProvider.DynamicClassProvider;
import net.jueb.util4j.hotSwap.classSources.DefaultClassSource;

public class ServerConfig {
	public static final Environment env=Environment.getInstance();
	public static String CONFIGURATION_FILE = "config.properties";
	public static JProperties SERVER_SETTINGS;
	public static boolean DEBUG;
	public static int SERVER_ID=4000;
	public static boolean AUTO_RELOAD;
	public static String BIND_HOST;
	public static int BIND_PORT;
	
	public static String LAN_HOST;
	public static int LAN_PORT;
	
	public static int CENTER_PORT;
	public static String CENTER_HOST;
	
	public static String TOKEN_API;
	
	private static String SCRIPT_PROJECT_NAME = "majong-script-game";
	public static String SCRIPT_SOURCE_DIR;// 脚本资源目录
	public static DefaultClassSource classSource;
	public static DynamicClassProvider classProvider;
	public static Runnable reloadTask;
	
	static Logger log;
	
	static{
		Log4jUtil.initLogConfig(env.getLogConfigFile());
		log=Log4jUtil.getLogger(ServerConfig.class);
	}

	public static void loadConfig() throws Exception {
		SERVER_SETTINGS = new JProperties(env.getConfDir() + "/" + CONFIGURATION_FILE);
		DEBUG = Boolean.parseBoolean(SERVER_SETTINGS.getProperty("DEBUG"));
		SERVER_ID = Integer.parseInt(SERVER_SETTINGS.getProperty("SERVER_ID"));
		AUTO_RELOAD = Boolean.parseBoolean(SERVER_SETTINGS.getProperty("AUTO_RELOAD"));
		
		BIND_HOST = SERVER_SETTINGS.getProperty("BIND_HOST");
		BIND_PORT = Integer.parseInt(SERVER_SETTINGS.getProperty("BIND_PORT"));
		
		LAN_HOST = SERVER_SETTINGS.getProperty("LAN_HOST");
		LAN_PORT = Integer.parseInt(SERVER_SETTINGS.getProperty("LAN_PORT"));
		
		CENTER_HOST = SERVER_SETTINGS.getProperty("CENTER_HOST");
		CENTER_PORT = Integer.parseInt(SERVER_SETTINGS.getProperty("CENTER_PORT"));
		TOKEN_API =SERVER_SETTINGS.getProperty("TOKEN_API");
	

		SCRIPT_SOURCE_DIR = SERVER_SETTINGS.getProperty("SCRIPT_SOURCE_DIR");
		if (env.getClass() == EnvironmentType.Dev_Maven.getEnvImpl()) {// 开发环境
			String rootPath = new File(System.getProperty("user.dir")).getParentFile().getPath();
			SCRIPT_SOURCE_DIR = rootPath + File.separator + "ethPet-script" + File.separator + SCRIPT_PROJECT_NAME + File.separator + "target" + File.separator + "classes";
		} else {// 部署环境
			if (StringUtils.isBlank(SCRIPT_SOURCE_DIR)) {
				SCRIPT_SOURCE_DIR = env.getRootDir() + File.separator + "script";
			}
		}
		log.info("加载脚本目录:" + SCRIPT_SOURCE_DIR);
		classSource = new DefaultClassSource();
		classSource.addClassDir(new File(SCRIPT_SOURCE_DIR).toURI());
		classSource.addJarDir(new File(SCRIPT_SOURCE_DIR).toURI());
		classSource.updateAttach(ServerMain.scheduExec, TimeUnit.SECONDS, 10);
		classProvider = new DynamicClassProvider(classSource, AUTO_RELOAD);
		reloadTask = classProvider::reload;
	}
}
