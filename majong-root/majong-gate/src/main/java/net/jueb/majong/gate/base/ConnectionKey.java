package net.jueb.majong.gate.base;

public class ConnectionKey {
	
	public static final String RoleAgent="RoleAgent";
	
	public static final String GameServerId="GameServerId";
	
	public static final String NetClient="NetClient";
}
