package net.jueb.majong.game.factory;

import net.jueb.majong.core.script.IServerScript;
import net.jueb.majong.core.script.ServerScriptProvider;
import net.jueb.majong.game.ServerConfig;
import net.jueb.util4j.hotSwap.classProvider.IClassProvider;

public class ScriptFactory extends ServerScriptProvider<IServerScript>{

	protected ScriptFactory(IClassProvider classProvider) {
		super(classProvider);
	}

	private static ScriptFactory factory;
	
	public static ScriptFactory getInstance()
	{
		if(factory==null)
		{
			synchronized (ScriptFactory.class) {
				if(factory==null)
				{
					factory=new ScriptFactory(ServerConfig.classProvider);
				}
			}
		}
		return factory;
	}
}
