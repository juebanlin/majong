package net.jueb.majong.core.net.server;

public interface NetServer {
	
	public void startNetWork() throws Exception;

	public void stop();
}
