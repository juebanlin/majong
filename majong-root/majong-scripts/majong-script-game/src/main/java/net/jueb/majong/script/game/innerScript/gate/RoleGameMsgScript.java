package net.jueb.majong.script.game.innerScript.gate;

import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.core.net.message.GameErrCode;
import net.jueb.majong.core.net.message.GameMessage;
import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.net.message.RoleGameMessage;
import net.jueb.majong.core.script.IServerScript;
import net.jueb.majong.core.script.factory.annations.IntMapper;
import net.jueb.majong.game.ServerConfig;
import net.jueb.majong.game.base.RoleController;
import net.jueb.majong.game.factory.ScriptFactory;
import net.jueb.majong.game.manager.RoleManager;
import net.jueb.majong.script.game.innerScript.AbstractInnerScript;

/**
 * 处理服务器的代理消息并发往客户端
 * @author Administrator
 */
@IntMapper(GameMsgCode.Gate_RoleGameMessage)
public class RoleGameMsgScript extends AbstractInnerScript{

	//发送代理消息
	@Override
	public void action() {
		NetConnection conn=(NetConnection) getParam(0);
		RoleGameMessage msg=(RoleGameMessage) getParam(1);
		msg.setServerId(ServerConfig.SERVER_ID);
		conn.sendMessage(msg.wrapGameMessage(GameMsgCode.Gate_RoleGameMessage));//包装到Gate_RoleGameMessage消息中
		_log.trace("Send Gate RoleGameMessage="+msg);
	}
	
	/**
	 *收到代理消息
	 */
	@Override
	protected void handleRequest(NetConnection connection, ByteBuffer clientBuffer) {
		RoleGameMessage rmsg=new RoleGameMessage();
		rmsg.readFrom(clientBuffer);
		long roleId=rmsg.getRoleId();
		GameMessage msg=rmsg.getMsg();
		int pcode=msg.getCode();
		RoleController role=RoleManager.getInstance().getById(roleId);
		if(role==null)
		{
			_log.error("角色不存在当前服务器,无法执行代理消息,Gate RoleGameMessage="+msg);
			sendGameErrorInfo(connection, roleId, GameErrCode.RoleNotFound);
			//通知大厅此玩家解锁
			noticeCenterUnlockIfLock(roleId);
			return ;
		}
		_log.trace("Revice Gate Gate RoleGameMessage="+msg);
		IServerScript script=ScriptFactory.getInstance().buildHandleRequest(pcode, connection,rmsg);
		if(script!=null)
		{
			script.run();
		}
		_log.trace("Handle Gate RoleGameMessage="+msg+",script="+script);
	}
}
