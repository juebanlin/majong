package net.jueb.majong.script.game.role;

import org.apache.commons.lang.math.RandomUtils;

import net.jueb.majong.core.common.enums.GameRoomEvent;
import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.core.net.message.GameErrCode;
import net.jueb.majong.core.net.message.GameMessage;
import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.net.message.RoleGameMessage;
import net.jueb.majong.core.script.factory.annations.IntMapper;
import net.jueb.majong.game.base.RoleController;
import net.jueb.majong.game.base.RoomController;
import net.jueb.majong.game.base.TableLocationEnum;
import net.jueb.majong.game.manager.RoleManager;
import net.jueb.majong.game.manager.RoomManager;

/**
 * 
 */
@IntMapper(GameMsgCode.Game_CreateRoom)
public class CreateRoomScript extends AbstractRoleGameActionScript{

	@Override
	public void action() {
		
	}
	
	@Override
	protected void handleAction(NetConnection connection, RoleGameMessage action) {
		RoleController role=RoleManager.getInstance().getById(action.getRoleId());
		if(role==null)
		{
			_log.error("role not found by RoleGameMessage:"+action);
			return ;
		}
		if(role.getRoom()!=null)
		{
			ByteBuffer buffer=new ByteBuffer();
			buffer.writeInt(GameErrCode.InOtherRoomError.value());
			responseAction(new GameMessage(GameMsgCode.Game_CreateRoom,buffer));
			return ;
		}
		RoomController room=RoomManager.getInstance().createRoom();
		if(room==null)
		{
			ByteBuffer buffer=new ByteBuffer();
			buffer.writeInt(GameErrCode.ServerRoomLimit.value());
			responseAction(new GameMessage(GameMsgCode.Game_CreateRoom,buffer));
			return ;
		}
		int rand=RandomUtils.nextInt(TableLocationEnum.values().length);
		TableLocationEnum location=TableLocationEnum.valueOf(rand);
		room.setRole(role, location);//设置玩家位置
		room.setMaster(location);//设置房主
		room.setBanker(location);//设置庄家
		//TODO 设置房间规则
		//回复房间创建完毕,以及房间相关信息
		ByteBuffer buffer=new ByteBuffer();
		buffer.writeInt(GameErrCode.Succeed.value());
		buffer.writeUTF(room.getNumber().toNumberString());//房间号
		buffer.writeInt(location.getValue());//房间位置
		responseAction(new GameMessage(GameMsgCode.Game_CreateRoom,buffer));
		push_RoomEvent(room.getNumber(), GameRoomEvent.Create, role.getId());
	}
}