package net.jueb.majong.script.game.innerScript.gate;

import net.jueb.majong.core.common.ServerInfo;
import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.core.net.message.GameErrCode;
import net.jueb.majong.core.net.message.GameMessage;
import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.script.factory.annations.IntMapper;
import net.jueb.majong.game.ServerConfig;
import net.jueb.majong.game.base.GateController;
import net.jueb.majong.game.manager.GateManager;
import net.jueb.majong.script.game.innerScript.AbstractInnerScript;

/**
 * 网关注册
 * @author Administrator
 */
@IntMapper(GameMsgCode.Game_GateReg)
public class GateRegScript extends AbstractInnerScript{


	@Override
	protected void handleRequest(NetConnection connection, ByteBuffer clientBuffer) {
		GameErrCode code=GameErrCode.UnknownError;
		ServerInfo info=new ServerInfo();
		info.readFrom(clientBuffer);
		_log.info("网关申请注册:"+info);
		if(GateManager.getInstance().find(connection)!=null)
		{
			_log.warn("网关重复发起注册请求:"+connection);
			response(connection,GameErrCode.Succeed);
			return;
		}
		GateController gate=GateManager.getInstance().get(info.getServerId());
		if(gate!=null)
		{
			code=GameErrCode.RepeatServerIdRegError;
			_log.error("网关注册失败,已存在相同ID网关:gate="+gate+",info="+info);
			response(connection,code);
			return;
		}
		GateManager.getInstance().regist(info,connection);
		response(connection,GameErrCode.Succeed);
		_log.info("网关注册成功:"+info);
	}
	
	private void response(NetConnection connection,GameErrCode code)
	{
		GameMessage msg=new GameMessage(GameMsgCode.Game_GateReg);
		msg.getContent().writeInt(code.value());
		msg.getContent().writeInt(ServerConfig.SERVER_ID);
		connection.sendMessage(msg);
	}

	@Override
	public void action() {
		
	}
}
