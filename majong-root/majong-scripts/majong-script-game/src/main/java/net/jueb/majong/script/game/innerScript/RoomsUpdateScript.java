package net.jueb.majong.script.game.innerScript;

import java.util.Collection;

import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.script.IServerScript;
import net.jueb.majong.core.script.factory.annations.IntMapper;
import net.jueb.majong.game.ServerMain;
import net.jueb.majong.game.base.RoomController;
import net.jueb.majong.game.factory.ScriptFactory;
import net.jueb.majong.game.manager.RoomManager;

/**
 * 更新所有房间
 * @author jaci
 */
@IntMapper(GameMsgCode.Game_RoomsUpdate)
public class RoomsUpdateScript extends AbstractInnerScript{

	@Override
	public void action() {
		Collection<RoomController> rooms=	RoomManager.getInstance().getList();
		for(RoomController r:rooms)
		{
			IServerScript script=ScriptFactory.getInstance().buildAction(GameMsgCode.Game_RoomUpdate, r);
			short roomId=(short) r.getNumber().getRoomId();
			ServerMain.getInstance().getQueues().execute(roomId, script);
		}
	}

	@Override
	protected void handleRequest(NetConnection connection, ByteBuffer clientBuffer) {
		// TODO Auto-generated method stub
	}
}
