package net.jueb.majong.script.game.innerScript;

import java.util.concurrent.TimeUnit;

import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.script.factory.annations.IntMapper;
import net.jueb.majong.game.base.RoleController;
import net.jueb.majong.game.base.RoomController;
import net.jueb.majong.game.factory.ScriptFactory;

/**
 * 更新单个房间
 * @author jaci
 */
@IntMapper(GameMsgCode.Game_RoomUpdate)
public class RoomUpdateScript extends AbstractInnerScript{

	/**
	 * 房间最大保留秒数
	 */
	public long maxRoomAliveTime=TimeUnit.MINUTES.toMillis(10);
	
	@Override
	public void action() {
		RoomController room=(RoomController) getParam(0);
		switch (room.getState()) {
		case Created:
		{
			RoleController master=room.getRole(room.getMaster());
			if(!master.isOnline() && room.getRoles().size()==1)
			{//只有房主一个人的时候
				long time=System.currentTimeMillis()-master.getLastOffLineTime();
				if(time>=maxRoomAliveTime)
				{//解散房间并让房主离开游戏服务器
					ScriptFactory.getInstance().buildAction(GameMsgCode.Game_DisbandRoom, room).run();
					ScriptFactory.getInstance().buildAction(GameMsgCode.Game_LeaveGame, master).run();
				}
			}
		}
			break;

		default:
			break;
		}
	}

	@Override
	protected void handleRequest(NetConnection connection, ByteBuffer clientBuffer) {
	}
}
