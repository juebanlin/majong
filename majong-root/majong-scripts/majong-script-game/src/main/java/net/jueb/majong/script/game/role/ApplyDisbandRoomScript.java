package net.jueb.majong.script.game.role;

import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.core.net.message.GameErrCode;
import net.jueb.majong.core.net.message.GameMessage;
import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.net.message.RoleGameMessage;
import net.jueb.majong.core.script.factory.annations.IntMapper;
import net.jueb.majong.game.base.RoleController;
import net.jueb.majong.game.base.RoomController;
import net.jueb.majong.game.manager.RoleManager;

/**
 * 玩家申请解散房间
 */
@IntMapper(GameMsgCode.Game_ApplyDisbandRoom)
public class ApplyDisbandRoomScript extends AbstractRoleGameActionScript{
	
	@Override
	public void action() {
		
	}
	
	@Override
	protected void handleAction(NetConnection connection, RoleGameMessage action) {
		RoleController role=RoleManager.getInstance().getById(action.getRoleId());
		if(role==null)
		{
			_log.error("role not found by RoleGameMessage:"+action);
			return ;
		}
		RoomController room=role.getRoom();
		if(room==null)
		{//房间不存在
			ByteBuffer rsp=new ByteBuffer();
			rsp.writeInt(GameErrCode.UnSupportOperation.value());
			responseAction(new GameMessage(GameMsgCode.Game_ApplyDisbandRoom,rsp));
			return ;
		}
		//TODO 预留接口
	}
}