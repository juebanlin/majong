package net.jueb.majong.script.game.innerScript.center;

import net.jueb.majong.core.common.dto.RoleChange;
import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.core.net.message.GameMessage;
import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.script.factory.annations.IntMapper;
import net.jueb.majong.game.ServerMain;
@IntMapper(GameMsgCode.Game_RoleDataUpdate)
public class RoleDataUpdateScript extends AbstractCenterScript{

	@Override
	public void action() {
		RoleChange change=(RoleChange) getParam(0);
		ByteBuffer buff=new ByteBuffer();
		change.writeTo(buff);
		ServerMain.getInstance().getCenterClient().sendMessage(new GameMessage(GameMsgCode.Game_RoleDataUpdate,buff));
	}

	@Override
	protected void handleRequest(NetConnection connection, ByteBuffer clientBuffer) {
		
	}
}
