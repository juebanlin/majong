package net.jueb.majong.script.game.innerScript.center;

import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.core.net.message.GameMessage;
import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.script.factory.annations.IntMapper;
import net.jueb.majong.game.ServerConfig;
import net.jueb.majong.game.ServerMain;

/**
 * 玩家离开游戏服
 */
@IntMapper(GameMsgCode.Center_GameRoleExit)
public class RoleExitScript extends AbstractCenterScript{

	@Override
	public void action() {
		long roleId=(long) getParam(0);
		ByteBuffer buffer=new ByteBuffer();
		buffer.writeInt(ServerConfig.SERVER_ID);
		buffer.writeLong(roleId);
		ServerMain.getInstance().getCenterClient().sendMessage(new GameMessage(GameMsgCode.Center_GameRoleExit, buffer));
		_log.debug("通知大厅玩家离开游戏服,roleId="+roleId);
	}

	@Override
	protected void handleRequest(NetConnection connection, ByteBuffer clientBuffer) {
		
	}
}
