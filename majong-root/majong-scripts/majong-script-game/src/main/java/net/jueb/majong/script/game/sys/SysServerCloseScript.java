package net.jueb.majong.script.game.sys;

import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.script.factory.annations.IntMapper;

/**
 * 凌晨任务
 * @author Administrator
 */
@IntMapper(GameMsgCode.Sys_ServerClose)
public class SysServerCloseScript extends AbstractSysScript{

	@Override
	public void action() {
		_log.warn("关服脚本执行…………");
		_log.warn("关服脚本执行完毕");
	}
}
