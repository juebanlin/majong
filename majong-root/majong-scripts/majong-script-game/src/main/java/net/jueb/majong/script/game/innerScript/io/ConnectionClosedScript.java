package net.jueb.majong.script.game.innerScript.io;

import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.script.IServerScript;
import net.jueb.majong.core.script.factory.annations.IntMapper;
import net.jueb.majong.game.ServerMain;
import net.jueb.majong.game.ServerQueue;
import net.jueb.majong.game.base.GateController;
import net.jueb.majong.game.factory.ScriptFactory;
import net.jueb.majong.game.manager.GateManager;
import net.jueb.majong.script.game.innerScript.AbstractInnerScript;

@IntMapper(GameMsgCode.Game_ConnectionClosed)
public class ConnectionClosedScript extends AbstractInnerScript{

	@Override
	public void action() {
		NetConnection connection=(NetConnection) getParam(0);
		GateController gate=GateManager.getInstance().find(connection);
		if(gate!=null)
		{
			IServerScript script=ScriptFactory.getInstance().buildAction(GameMsgCode.Game_GateUnReg, gate);
			ServerMain.getInstance().getQueues().execute(ServerQueue.MAIN,script);
		}
	}

	@Override
	protected void handleRequest(NetConnection connection, ByteBuffer clientBuffer) {
		// TODO Auto-generated method stub
	}
}
