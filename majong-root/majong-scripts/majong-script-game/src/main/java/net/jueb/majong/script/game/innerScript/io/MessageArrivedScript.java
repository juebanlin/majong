package net.jueb.majong.script.game.innerScript.io;

import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.core.net.message.GameMessage;
import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.script.IServerScript;
import net.jueb.majong.core.script.factory.annations.IntMapper;
import net.jueb.majong.game.ServerMain;
import net.jueb.majong.game.ServerQueue;
import net.jueb.majong.game.factory.ScriptFactory;
import net.jueb.majong.script.game.innerScript.AbstractInnerScript;

/**
 * @author Administrator
 */
@IntMapper(GameMsgCode.Game_MessageArrived)
public class MessageArrivedScript extends AbstractInnerScript{

	@Override
	public void action() {
		NetConnection conn=(NetConnection) getParam(0);
		GameMessage msg=(GameMessage) getParam(1);
		int code =msg.getCode();
		//系统内部消息范围1000-1999
		//大厅范围20000-2999;
		//游戏范围30000-3999;
		//网关消息范围4000-4999;
		IServerScript script=null;
		ByteBuffer byteBuffer = msg.getContent();
		short queue =ServerQueue.MAIN;
		switch (code) {
		default:
			break;
		}
		script =ScriptFactory.getInstance().buildHandleRequest(code,conn,byteBuffer);
		if(script!=null)
		{
			ServerMain.getInstance().getQueues().execute(queue, script);
		}else
		{
			_log.error("recvMsg,code="+code+"(0x"+Integer.toHexString(code)+"),script:"+script);
		}
	}

	@Override
	protected void handleRequest(NetConnection connection, ByteBuffer clientBuffer) {
		// TODO Auto-generated method stub
	}
}
