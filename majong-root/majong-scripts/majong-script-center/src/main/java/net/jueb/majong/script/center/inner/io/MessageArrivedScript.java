package net.jueb.majong.script.center.inner.io;

import net.jueb.majong.center.ServerMain;
import net.jueb.majong.center.ServerQueue;
import net.jueb.majong.center.factory.ScriptFactory;
import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.core.net.message.GameMessage;
import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.script.IServerScript;
import net.jueb.majong.core.script.factory.annations.IntMapper;
import net.jueb.majong.script.center.inner.AbstractInnerScript;

/**
 * @author Administrator
 */
@IntMapper(GameMsgCode.Center_MessageArrived)
public class MessageArrivedScript extends AbstractInnerScript{

	@Override
	public void action() {
		NetConnection conn=getParam(0);
		GameMessage msg=getParam(1);
		int code =msg.getCode();
		ByteBuffer byteBuffer = msg.getContent();
		IServerScript script=null;
		short queue =ServerQueue.MAIN;
		switch (code) {
			case GameMsgCode.Center_UserLogin:
				queue=ServerQueue.LOGIN;
				break;
		default:
			break;
		}
		script =ScriptFactory.getInstance().buildHandleRequest(code, conn,byteBuffer);
		if(script!=null)
		{
			ServerMain.getInstance().getQueues().execute(queue, script);
			_log.trace("recvMsg,code="+code+"(0x"+Integer.toHexString(code)+"),conn="+conn+",script:"+script);
		}else
		{
			_log.error("recvMsg,code="+code+"(0x"+Integer.toHexString(code)+"),script:"+script);
		}
	}

	@Override
	protected void handleRequest(NetConnection connection, ByteBuffer clientBuffer) {
		// TODO Auto-generated method stub
	}
}
