package net.jueb.majong.script.center.inner;

import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.script.center.AbstractCenterBaseScript;

public abstract class AbstractInnerScript extends AbstractCenterBaseScript implements IInnerScript {

	@Override
	public final void handleRequest(Request request) {
		handleRequest(request.getConnection(), (ByteBuffer) request.getContent());
	}
	
	protected abstract void handleRequest(NetConnection connection, ByteBuffer msg);
	
	@Override
	public void action() {
		
	}
}