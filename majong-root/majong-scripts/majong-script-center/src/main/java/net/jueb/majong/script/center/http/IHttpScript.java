package net.jueb.majong.script.center.http;

import io.netty.handler.codec.http.HttpRequest;
import net.jueb.majong.core.script.IServerScript;

public interface IHttpScript extends IServerScript{

	void handleRequest(HttpRequest request);
}
