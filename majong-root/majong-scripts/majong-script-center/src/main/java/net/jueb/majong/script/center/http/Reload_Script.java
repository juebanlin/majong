package net.jueb.majong.script.center.http;

import io.netty.handler.codec.http.HttpRequest;
import net.jueb.majong.center.ServerConfig;
import net.jueb.majong.center.ServerMain;
import net.jueb.majong.core.script.factory.annations.StringMapper;
import net.jueb.util4j.hotSwap.classProvider.DynamicClassProvider;

@StringMapper("/reload")
public class Reload_Script extends AbstractHttpScript{

	@Override
	public void handleRequest(HttpRequest request) {
		DynamicClassProvider classProvider=ServerConfig.classProvider;
		boolean isAutoReload = classProvider.isAutoReload();
		if (isAutoReload) {
			responseMsg(0);
		} else {
			//脚本里面不可直接重载,会导致当前脚本classloader持有下一代classloader的脚本,所以需要异步
			ServerConfig.classSource.scanClassSources();
			ServerMain.scheduExec.execute(ServerConfig.reloadTask);
			responseMsg(1);
		}
	}
}