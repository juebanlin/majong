package net.jueb.majong.script.center.inner.gate;

import net.jueb.majong.center.base.RoleCache;
import net.jueb.majong.center.manager.RoleGateLockManager;
import net.jueb.majong.core.common.dto.RoleGateEventMsg;
import net.jueb.majong.core.common.dto.RoleLock.LockServerType;
import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.script.factory.annations.IntMapper;
import net.jueb.majong.script.center.inner.AbstractInnerScript;

@IntMapper(GameMsgCode.Gate_RoleGateEvent)
public class RoleGateEventScript extends AbstractInnerScript{

	@Override
	public void action() {
		
	}
	
	/**
	 *收到代理消息
	 */
	@Override
	protected void handleRequest(NetConnection connection, ByteBuffer clientBuffer) {
		RoleGateEventMsg msg=new RoleGateEventMsg();
		msg.readFrom(clientBuffer);
		long roleId=msg.getRoleId();
		if(RoleCache.getInstance().getRoleById(roleId)==null)
		{
			_log.error("Role Not found from Revice Gate RoleGateEventMsg="+msg);
			return ;
		}
		_log.debug("收到玩家网关事件:"+msg);
		switch (msg.getEvent()) {
		case Login://锁定角色
			RoleGateLockManager.getInstance().lock(roleId, msg.getServerId());
			broadcastRoleLockUpdate(msg.getServerId(), LockServerType.Gate, true, roleId);
			break;
		case LogOut:
			RoleGateLockManager.getInstance().unLock(roleId);
			broadcastRoleLockUpdate(msg.getServerId(), LockServerType.Gate, false, roleId);
			break;
		default:
			break;
		}
	}
}
