package net.jueb.majong.script.gate.center;

import net.jueb.majong.core.common.dto.UserLoginInfo;
import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.core.net.message.GameMessage;
import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.script.factory.annations.IntMapper;
import net.jueb.majong.gate.ServerMain;
import net.jueb.util4j.cache.callBack.CallBack;

@IntMapper(GameMsgCode.Center_UserLogin)
public class UserLoginScript extends AbstractCenterScript{

	@SuppressWarnings("unchecked")
	@Override
	public void action() {
		UserLoginInfo user=(UserLoginInfo) getParam(0);
		CallBack<ByteBuffer> callBack=(CallBack<ByteBuffer>) getParam(1);
		long callKey=ServerMain.getInstance().getCallBackCache().put(callBack,CallBack.DEFAULT_TIMEOUT);
		GameMessage msg=new GameMessage(GameMsgCode.Center_UserLogin);
		msg.getContent().writeLong(callKey);
		user.writeTo(msg.getContent());
		ServerMain.getInstance().getCenterClient().sendMessage(msg);
	}


	@Override
	protected void handleRequest(NetConnection connection, ByteBuffer clientBuffer) {
		ByteBuffer buffer=clientBuffer;
		long callKey=buffer.readLong();
		CallBack<ByteBuffer> callBack=ServerMain.getInstance().getCallBackCache().poll(buffer, callKey);
		if(callBack!=null)
		{
			callBack.call(buffer);
		}
	}
}
