package net.jueb.majong.script.gate.center;

import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.script.factory.annations.IntMapper;
import net.jueb.majong.gate.manager.GameConnectionManager;

@IntMapper(GameMsgCode.Center_GameUnReg)
public class GameUnRegScript extends AbstractCenterScript{

	@Override
	public void action() {
		
	}

	@Override
	protected void handleRequest(NetConnection connection, ByteBuffer clientBuffer) {
		ByteBuffer buffer=clientBuffer;
		int serverId=buffer.readInt();
		_log.info("游戏服务器离线:"+serverId);
		NetConnection conn=GameConnectionManager.getInstance().removeServerConnection(serverId);
		if(conn!=null && conn.isActive())
		{
			conn.close();
			_log.info("断开网关与游戏服务器的链接:"+serverId);
		}
	}
}
