package net.jueb.majong.script.gate.game;

import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.script.gate.AbstractGateBaseScript;

public abstract class AbstractGameScript extends AbstractGateBaseScript implements IGameScript{

	
	@Override
	public final void handleRequest(Request request) {
		handleRequest(request.getConnection(), (ByteBuffer) request.getContent());
	}
	
	protected abstract void handleRequest(NetConnection connection, ByteBuffer msg);
}
