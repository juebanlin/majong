package net.jueb.majong.script.gate.center;

import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.core.net.message.GameMessage;
import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.script.factory.annations.IntMapper;
import net.jueb.majong.gate.manager.RoleAgentManager;
import net.jueb.util4j.buffer.BytesBuff;

@IntMapper(GameMsgCode.Gate_Broadcast)
public class GateBroadcastScript extends AbstractCenterScript{

	@Override
	public void action() {
	}

	@Override
	protected void handleRequest(NetConnection connection, ByteBuffer clientBuffer) {
		int code=clientBuffer.readInt();
		int dataSize=clientBuffer.readInt();
		BytesBuff data=clientBuffer.readBytes(dataSize);
		GameMessage msg=new GameMessage(code,new ByteBuffer(data.getBytes()));
		RoleAgentManager.getInstance().broadcast(msg);
		_log.debug("广播消息：code="+code);
	}
}
