package net.jueb.majong.script.gate.sys;

import java.text.SimpleDateFormat;
import java.util.Date;

import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.script.factory.annations.IntMapper;

/**
 * 凌晨任务
 * @author Administrator
 */
@IntMapper(GameMsgCode.Sys_Time24)
public class Sys_Time24Script extends AbstractSysScript{

	private SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS");
	

	@Override
	public void action() {
		_log.info("凌晨任务开始:"+sdf.format(new Date()));
	}
	
}
