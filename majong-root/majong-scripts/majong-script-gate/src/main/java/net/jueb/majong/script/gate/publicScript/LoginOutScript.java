package net.jueb.majong.script.gate.publicScript;

import net.jueb.majong.core.common.enums.RoleGateEvent;
import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.script.factory.annations.IntMapper;
import net.jueb.majong.gate.base.RoleAgent;

/**
 * 用户登出或者超时未登录
 * @author Administrator
 */
@IntMapper(GameMsgCode.Gate_LoginOut)
public class LoginOutScript extends AbstractPublicScript{

	@Override
	public void action() {
		RoleAgent ra=(RoleAgent) getParam(0);
		_log.debug("网关玩家登出:"+ra);
		roleGateEvent(ra,RoleGateEvent.LogOut);
	}

	@Override
	protected void handleRequest(NetConnection connection, ByteBuffer msg) {
		
	}
}
